const openBtn = document.querySelector(".button-container");
const closeBtn = document.querySelector(".side-nav__btn-close");

function openSlideMenu() {
  document.querySelector(".side-nav").style.width = "250px";
  document.querySelector(".side-nav__items").style.margin = "0 0 0 0";
}

function closeSlideMenu() {
  document.querySelector(".side-nav").style.width = "0";
  document.querySelector(".side-nav__items").style.margin = "0 0 0 -250px";
}

openBtn.addEventListener("click", openSlideMenu);
closeBtn.addEventListener("click", closeSlideMenu);

