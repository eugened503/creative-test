function getLargestSubstr(a, b) {
  let strMin = a < b ? a : b;
  let strMax = a > b ? [a] : [b];

  const res = (min, max) => {
    for (let l = min.length; l > 0; l--) {
      for (let p = 0; p <= min.length - l; p++) {
        let substr = min.slice(p, p + l);
        let isDetected = true;
        for (let i = 0; i < max.length; i++) {
          if (max[i].indexOf(substr) >= 0) continue;

          isDetected = false;
          break;
        }

        if (isDetected) return substr;
      }
    }

    return "";
  };

  console.log("Входные данные: ", a, b);
  console.log("Результат: ", res(strMin, strMax));
}

getLargestSubstr("aababba", "abbaabcd");
