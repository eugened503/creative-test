const emailCheckInput = document.querySelector(".checkbox-input");
const passwordCheckInput = document.querySelector(".checkbox-password");
const dateCheckInput = document.querySelector(".checkbox-date");
const allCheckInput = document.querySelectorAll(".form-check-toggle");
const emailFieldInput = document.querySelector(".input-email");
const passwordFieldInput = document.querySelector(".input-password");
const dateFieldInput = document.querySelector(".date-password");
const checks = document.querySelector(".checkbox-all");

function toggleEmail() {
  emailFieldInput.classList.toggle("hidden-div");
}

function togglePassword() {
  passwordFieldInput.classList.toggle("hidden-div");
}

function toggleDate() {
  dateFieldInput.classList.toggle("hidden-div");
}

function toggleAll() {
  if (checks.checked) {
    emailFieldInput.classList.remove("hidden-div");
    passwordFieldInput.classList.remove("hidden-div");
    dateFieldInput.classList.remove("hidden-div");
    allCheckInput.forEach((item) => (item.checked = true));
  } else {
    emailFieldInput.classList.add("hidden-div");
    passwordFieldInput.classList.add("hidden-div");
    dateFieldInput.classList.add("hidden-div");
    allCheckInput.forEach((item) => (item.checked = false));
  }
}

checks.addEventListener("change", toggleAll);
dateCheckInput.addEventListener("change", toggleDate);
emailCheckInput.addEventListener("change", toggleEmail);
passwordCheckInput.addEventListener("change", togglePassword);
